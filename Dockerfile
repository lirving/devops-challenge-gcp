FROM golang:1.17

WORKDIR /app

COPY test-app/go.mod test-app/go.sum ./
RUN go mod download


COPY ./test-app/cmd/ops-test-app ./
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /app/ops-test-app

EXPOSE 8080

CMD [ "./ops-test-app" ]
