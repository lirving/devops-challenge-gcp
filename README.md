# DevOps Challenge Solution 

This repository contains the documentation and code needed to provide an architecture for the `test-apps` in Google Cloud Platform. 


## Architecture


![architecture_1](architecture_1.png)

Test-app is a stateless application exposing a single endpoint `/health`.

This application will be deployed as a microservice in a container inside a Kubernetes Cluster. 
Hence, it will be easy to deploy, scale and secure it.

This setup contains:

* A GKE cluster with regional high availability.
  This GKE cluster is deployed as a VPC-native cluster with 2 dedicated ranges for pods and services.

* A node pool with autoscaling enabled to resize the cluster automatically according to the workload.

* Cloud SQL PostgresDB instance with Region high availability.
  Cloud SQL instances are configured with a private ip in the VPC.

The Application `test-app` is deployed as Kubernetes Deployment with autoscaling managed by the HPA. 

To connect to the DB, test-app uses Cloud SQL proxy. 
Cloud SQL proxy handles the connection with TLS encryption and the authorization with IAM.

All infrastructure components are provisioned with terraform. 

TF code can be found in [terraform/](terraform/)`. 

For a complete view of resources and variables see [here](terraform/README.md)

The application is bundled in helm chart in the folder [helm](helm/).

## Requirements 

### Tools  

`terraform`>= 1.3

`kubectl`>= 1.24

`gcloud`>= 400

Configure gcloud cli with your project and region , see https://cloud.google.com/sdk/docs/install-sdk

### IAM


* Enable the following GCP APIs in your project: 

   * Service Networking API 
   * Cloud Logging API
   * Compute Engine API
   * Cloud Monitoring API
   * Kubernetes Engine API
   * Cloud Resource Manager API
   * Cloud SQL Admin API
   * Identity and Access Management (IAM) API
   * Service Networking API

* Create a dedicated Service Account

  
  To limit the permissions we create a service account which will be used for creating resources with terraform.
  ```
  gcloud iam service-accounts create terraform \
      --description="Service Account for Terraform" \
      --display-name="terraform"
  ```

* Add the following roles to the SA with gcloud or GCP console 

    ```
    roles/compute.viewer
    roles/compute.securityAdmin
    roles/container.clusterAdmin
    roles/container.developer
    roles/iam.serviceAccountAdmin
    roles/iam.serviceAccountUser
    roles/resourcemanager.projectIamAdmin
    roles/compute.networkadmin
    roles/cloudsql.admin
    ```
    
    e.g
    
    ```
    gcloud projects add-iam-policy-binding <PROJECT_ID>  \
    --member=serviceAccount:terraform@<PROJECT_ID>.iam.gserviceaccount.com --role <ROLE>
    ```

* Create the GCS bucket for tfstate storage and give SA permission to use it
  ```
  gcloud storage buckets create gs://<BUCKETNAME>
  gcloud storage buckets add-iam-policy-binding gs://<BUCKETNAME> \
   --member=serviceAccount:terraform@<PROJECT_ID>.iam.gserviceaccount.com --role=roles/storage.objectAdmin
  ```
  Change the bucket name in the tfstate in `main.tf` 


*  Give your account permission to impersonate the service account  
    ```
    gcloud iam service-accounts add-iam-policy-binding \
        $GOOGLE_IMPERSONATE_SERVICE_ACCOUNT \
        --member="user:$USER_ACCOUNT_ID" \
        --role="roles/iam.serviceAccountTokenCreator"
    ```

## How to deploy

The Docker image is automatically built by the Gitlab CI pipeline.

Take the last image tag available and report it in `app_version` in terraform.tfvars (or use latest).

Inside `terraform/`, 

Run `terraform apply` (this step can take more than 10min)

Once completed you can login in the gke cluster with 

```gcloud container clusters get-credentials <GKE_NAME> --region <region>  ```

Get the public ip of the LB and try an HTTP request to validate the deployment.
```
curl http://$(kubectl get ingress -lapp.kubernetes.io/name=app-test -ojsonpath='{.items[].status.loadBalancer.ingress[].ip}')/health
``` 

## Scaling


The Horizontal Pod autoscaler scale the app-test deployment by watching the CPU average usage.

HPA settings :
```
  minReplicas: 1
  maxReplicas: 6
  targetCPUUtilizationPercentage: 40
```

Once the deployment CPU usage average reaches 40% the HPA scale another replica unless `maxReplicas` is reached. This is a simple way to scale the application.

If the cluster cannot schedule a new replica then a new node will be provisioned inside the node pool unless the max_node limit is reached.

To validate the scalibility we can simulate load with `wrk` 

``` 
wrk -t5 -c50 -d300s http://$(kubectl get ingress -lapp.kubernetes.io/name=app-test -ojsonpath='{.items[].status.loadBalancer.ingress[].ip}')/health
```

**Limitations** 

  The CloudSQL DB instance can be scale vertically but not horizontally.
  
  To help splitting load across multiple DB instance we could use read replicas.
  
  To do so we would need to modify app-test to support a supplementary read only connection.
  
  Cloud SQL proxy already manage multiple instances connections

  Architecture with Read replica:

  ![architecture_2](architecture_2.png)
 

## CI/CD 

Any changes on the test-app/ code will trigger a new image build.

The CI is configured inside Gitlab CI `.gitlab-ci.yml`.

The docker stage build  docker image and store in the Gitlab project registry.

A terraform linter,syntax validation could be added as part of this pipeline.

Continous Deployment is not handle in this repository.

We could be to reuse the helm chart with a GitOps tool such as ArgoCD and Flux.

For a better understanding of upgrade impact, we could use a Blue-Green or Canary deployment strategy.

# Monitoring

Monitoring and Alerting is not implemented in this repository.

A good starting point would be to implement client side monitoring.

Cloud-sql-proxy expose the following prometheus metrics:

    cloudsqlconn/dial_latency: The distribution of dialer latencies (ms)
    cloudsqlconn/open_connections: The current number of open Cloud SQL connections
    cloudsqlconn/dial_failure_count: The number of failed dial attempts
    cloudsqlconn/refresh_success_count: The number of successful certificate refresh operations
    cloudsqlconn/refresh_failure_count: The number of failed refresh operations.

To alert for application unavailability , we could create a public uptime check on http://<loadBalancerIp>/health.

We could also alert on the scaling limits when HPA reaches max capacity 

---

# FACEIT DevOps Challenge

You have been asked to create the infrastructure for running a web application on a cloud platform of your preference (Google Cloud Platform preferred, AWS or Azure are also fine).

The [web application](test-app/README.md) can be found in the `test-app/` directory. Its only requirements are to be able to connect to a PostgreSQL database and perform PING requests.    

The goal of the challenge is to demonstrate hosting, managing, documenting and scaling a production-ready system.

This is not about website content or UI.

## Requirements

- Deliver the tooling to set up the application using Terraform on the cloud platform of your choice (free tiers are fine)
- Provide basic architecture diagrams and documentation on how to initialise the infrastructure along with any other documentation you think is appropriate
- Provide and document a mechanism for scaling the service and delivering the application to a larger audience
- Describe a possible solution for CI and/or CI/CD in order to release a new version of the application to production without any downtime

Be prepared to explain your choices

## Extra Mile Bonus (not a requirement)

In addition to the above, time permitting, consider the following suggestions for taking your implementation a step further:

- Monitoring/Alerting
- Implement CI/CD (github actions, travis, circleci, ...)
- Security

## General guidance

- We recommend using this repository as a starting point, you can clone it and add your code/docs to that repository
- Please do no open pull request with your challenge against **this repository**
- Submission of the challenge can be done either via your own public repository or zip file containing `.git` folder

