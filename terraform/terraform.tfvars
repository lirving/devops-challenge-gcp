#--------------------------------------------------------------
# Google Cloud Plaftorm
#--------------------------------------------------------------

gcp_service_account = "terraform"
gcp_project = "example-385921"
gcp_region  = "europe-west4"

# GKE 
gke_name               = "devops-challenge"
gke_kubernetes_version = "1.26"
k8s_namespace = "default"

# VPC 
vpc_name                     = "devops-challenge"
vpc_subnetwork_name          = "europe-west4-01"
vpc_subnetwork_ip_cidr_range = "10.1.0.0/16"
vpc_subnetwork_secondary_ranges = {
  "gke-services" = {
    range_name    = "gke-services",
    ip_cidr_range = "192.168.64.0/18"
  }
  "gke-pods" = {
    range_name    = "gke-pods",
    ip_cidr_range = "192.168.0.0/18"
  }
 }

# Node pool with autoscaling

node_pool_name               = "default-e2-medium"
node_pool_machine_type       = "e2-medium"
node_pool_min_count          = 1
node_pool_max_count          = 6
node_pool_initial_node_count = 1
node_pool_disk_size          = 30
node_pool_disk_type          = "pd-standard"
node_pool_autoscaling        = true

# Cloud SQL PostgreSQL 
pg_name    = "test-apps-db"
pg_version = "POSTGRES_14"
pg_tier    = "db-g1-small"
pg_user = "foo"
pg_password = "foobar"

# Application version

app_version = "latest"