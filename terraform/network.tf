variable "vpc_name" { type = string }
variable "vpc_subnetwork_name" { type = string }
variable "vpc_subnetwork_ip_cidr_range" { type = string }
variable "vpc_subnetwork_secondary_ranges" {
  type = map(object({
    range_name    = string,
    ip_cidr_range = string
  }))
}

resource "google_compute_subnetwork" "devops-challenge" {
  name          = var.vpc_subnetwork_name
  ip_cidr_range = var.vpc_subnetwork_ip_cidr_range
  region        = var.gcp_region
  network       = google_compute_network.devops-challenge.id
  dynamic "secondary_ip_range" {
    for_each = var.vpc_subnetwork_secondary_ranges
    content {
      range_name    = secondary_ip_range.value["range_name"]
      ip_cidr_range = secondary_ip_range.value["ip_cidr_range"]
    }
  }
}

resource "google_compute_network" "devops-challenge" {
  name                    = var.vpc_name
  auto_create_subnetworks = false
}

resource "google_compute_global_address" "private" {
  name          = "private"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       =  google_compute_network.devops-challenge.self_link
}

resource "google_service_networking_connection" "private" {
  network                 = google_compute_network.devops-challenge.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private.name]
}

