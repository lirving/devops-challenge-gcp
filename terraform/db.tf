variable "pg_name" { type = string }
variable "pg_version" { type = string }
variable "pg_tier" { type = string }
variable "pg_user" { type = string }
variable "pg_password" { type = string }

locals {
  read_replica_ip_configuration = {
    ipv4_enabled       = false
    require_ssl        = true
    allocated_ip_range = null
    private_network    = google_compute_network.devops-challenge.id
    authorized_networks = []
  }
}

module "postgresql-db" {
  source               = "GoogleCloudPlatform/sql-db/google//modules/postgresql"
  version              = "15.0.0"
  name                 = var.pg_name
  random_instance_name = true
  project_id           = var.gcp_project
  database_version     = var.pg_version
  region               = var.gcp_region

  // Master configurations
  tier                            = var.pg_tier
  zone                            = "${var.gcp_region}-a"
  availability_type               = "REGIONAL"
  maintenance_window_day          = 7
  maintenance_window_hour         = 12
  maintenance_window_update_track = "stable"

  deletion_protection = false

  ip_configuration = {
    ipv4_enabled       = false
    require_ssl        = true
    private_network    = google_compute_network.devops-challenge.id
    allocated_ip_range = null
    authorized_networks = []
  }

  backup_configuration = {
    enabled                        = true
    start_time                     = "22:55"
    retained_backups               = 30
    retention_unit                 = "COUNT"
    transaction_log_retention_days = null
    point_in_time_recovery_enabled = false
    location                       = null

  }

  // Read replica configurations
#  read_replica_name_suffix = "-read-replica"
#  read_replicas = [
#    {
#      name                = "0"
#      zone                = "${var.gcp_region}-b"
#      availability_type   = "REGIONAL"
#      tier                = var.pg_tier
#      ip_configuration    = local.read_replica_ip_configuration
#      disk_size           = null
#      disk_autoresize       = null
#      disk_autoresize_limit = null
#      disk_type           = "PD_HDD"
#      database_flags        = [{ name = "autovacuum", value = "off" }]
#      user_labels         = { postgresql= "read-replica" }
#      encryption_key_name = null
#    }
#  ]

  db_name      = var.pg_name
  db_charset   = "UTF8"
  db_collation = "en_US.UTF8"

  user_name     = var.pg_user
  user_password = var.pg_password

  module_depends_on = [google_service_networking_connection.private.network]
}


