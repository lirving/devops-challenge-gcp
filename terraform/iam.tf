variable "k8s_namespace" { type = string }
resource "google_service_account" "test-app" {
  account_id   = "test-app-sa"
  display_name = "Cloud SQL Proxy sa for app-test "
  project      = var.gcp_project
  description  = "Used by test-app deployment"
}

resource "google_project_iam_member" "test-app-cloudsql-user" {
  role    = "roles/cloudsql.instanceUser"
  member  = "serviceAccount:${google_service_account.test-app.email}"
  project = var.gcp_project
}

resource "google_project_iam_member" "test-app-cloudsql-client" {
  role    = "roles/cloudsql.client"
  member  = "serviceAccount:${google_service_account.test-app.email}"
  project = var.gcp_project
}

resource "kubernetes_service_account" "cloud-sql-proxy" {
  metadata {
    name      =  "cloud-sql-proxy"
    namespace = var.k8s_namespace
    annotations = {
      "iam.gke.io/gcp-service-account" = "${google_service_account.test-app.email}"
    }
  }
secret {
    name = "${kubernetes_secret.cloud-sql-proxy.metadata.0.name}"
  }
}

resource "kubernetes_secret" "cloud-sql-proxy" {
  metadata {
    name = "cloud-sql-proxy-sa"
  }
}

resource "google_service_account_iam_binding" "ksa-bind" {
  service_account_id = google_service_account.test-app.id
  role               = "roles/iam.workloadIdentityUser"
  members = [
    "serviceAccount:${var.gcp_project}.svc.id.goog[${var.k8s_namespace}/${kubernetes_service_account.cloud-sql-proxy.metadata.0.name}]"
  ]
}
