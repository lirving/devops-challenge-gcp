# Terraform minimum version requirement
terraform {
  required_version = ">= 1.3.0"
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.51.0, < 5.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.10"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 2.1"
    }
    null = {
      source = "hashicorp/null"
      version = ">= 3"
    }
    helm = {
      source = "hashicorp/helm"
      version = ">= 2.9"
    }
  }
}
