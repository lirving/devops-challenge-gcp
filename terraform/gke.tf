variable "gke_name" { type = string }
variable "gke_kubernetes_version" { type = string }
variable "node_pool_name" { type = string }
variable "node_pool_machine_type" { type = string }
variable "node_pool_min_count" { type = number }
variable "node_pool_max_count" { type = number }
variable "node_pool_initial_node_count" { type = number }
variable "node_pool_disk_size" { type = number }
variable "node_pool_disk_type" { type = string }
variable "node_pool_autoscaling" { type =  bool }


# google_client_config and kubernetes provider must be explicitly specified like the following.
data "google_client_config" "default" {}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}

# Create GKE cluster with a VPC 
module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google"
  version                    = "25.0.0"
  project_id                 = var.gcp_project
  name                       = var.gke_name
  region                     = var.gcp_region
  regional                   = true
  create_service_account     = true
  network                    = google_compute_network.devops-challenge.name
  subnetwork                 = google_compute_subnetwork.devops-challenge.name
  ip_range_pods              = var.vpc_subnetwork_secondary_ranges["gke-pods"].range_name
  ip_range_services          = var.vpc_subnetwork_secondary_ranges["gke-services"].range_name
  remove_default_node_pool   = true
  http_load_balancing        = true
  network_policy             = false
  horizontal_pod_autoscaling = true
  filestore_csi_driver       = false
  kubernetes_version         = var.gke_kubernetes_version

  node_pools = [
    {
      name               = var.node_pool_name
      machine_type       = var.node_pool_machine_type
      min_count          = var.node_pool_min_count
      max_count          = var.node_pool_max_count
      local_ssd_count    = 0
      spot               = false
      disk_size_gb       = var.node_pool_disk_size
      disk_type          = var.node_pool_disk_type
      image_type         = "COS_CONTAINERD"
      enable_gcfs        = false
      enable_gvnic       = false
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = false
      initial_node_count = var.node_pool_initial_node_count
    },
  ]

  node_pools_oauth_scopes = {
    all = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }

  node_pools_labels = {
    all = {}
  }

  node_pools_metadata = {
    all = {}
  }

  node_pools_taints = {
    all = []
  }

  node_pools_tags = {
    all = []

    application = [
      var.gke_name,
    ]
  }
}

