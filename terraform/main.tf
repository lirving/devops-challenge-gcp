variable "gcp_region" { type = string }
variable "gcp_project" { type = string }
variable "gcp_service_account" { type = string }
variable "app_version" { type = string }
# Set up GCP provider
provider "google" {
  project         = var.gcp_project
  access_token    = data.google_service_account_access_token.terraform.access_token
  request_timeout = "60s"
}

terraform {
 backend "gcs" {
   bucket  =  "devops-challenge-terraform-tfstate"
   prefix  = "terraform/state"
 }
}


# Define impersonation provider
provider "google" {
  alias = "impersonation"
  scopes = [
    "https://www.googleapis.com/auth/cloud-platform",
    "https://www.googleapis.com/auth/userinfo.email"
  ]
}
# Retrieve SA token
data "google_service_account_access_token" "terraform" {
  provider               = google.impersonation
  target_service_account = local.terraform_service_account
  scopes                 = ["userinfo-email", "cloud-platform"]
  lifetime               = "1200s"
}

locals {
  terraform_service_account = "${var.gcp_service_account}@${var.gcp_project}.iam.gserviceaccount.com"
}


provider "helm" {
  kubernetes {
    host                   = "https://${module.gke.endpoint}"
    token                  = data.google_client_config.default.access_token
    cluster_ca_certificate = base64decode(module.gke.ca_certificate)
  }
}

# Application Deployment

resource "helm_release" "test-app" {
  name    = "test-app"
  chart   = "../helm"
  namespace = var.k8s_namespace
  timeout = 120
  values = [
    file("${path.module}/test-app-values.yaml")
  ]
  set {
   name  = "cloudproxy.instance_connection_name"
   value = module.postgresql-db.instance_connection_name
  }
 # set {
 #  name  = "cloudproxy.read_instance_connection_name"
 #  value = join(",",module.postgresql-db.replicas_instance_connection_names)
 # }
  set {
   name  = "config.POSTGRESQL_DBNAME"
   value = var.pg_name
  }
  set {
   name  = "config.POSTGRESQL_USER"
   value = var.pg_user
  }
  set {
   name  = "config.POSTGRESQL_PASSWORD"
   value = var.pg_password
  }
 set {
   name  = "image.tag"
   value = var.app_version
  }

  depends_on = [
    module.postgresql-db
  ]
}
